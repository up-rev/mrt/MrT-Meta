CRC 
===

This module provides utility functions for calculating cyclic redundancy check (CRC) values. Right now only CRC32 is supported as it is the most common, but more will be added as needed

Use 
---

The module can calculate CRCs on a buffer in one run, or in multiple chunks for larger buffers. 

Single Chunk 
~~~~~~~~~~~~

.. code:: c 

    #include "Utilities/CRC/crc32.h"


    uint32_t crc = crc32(buffer, buffer_length);


Multiple Chunks 
~~~~~~~~~~~~~~~

.. code:: c 

    #include "Utilities/CRC/crc32.h"

    crc32_ctx_t crc_ctx; 

    crc32_init(&crc_ctx);

    crc32_update(&crc_ctx, buf0, buf0_len);
    crc32_update(&crc_ctx, buf1, buf1_len);
    crc32_update(&crc_ctx, buf2, buf2_len);

    uint32_t crc = crc32_result(&crc_ctx);