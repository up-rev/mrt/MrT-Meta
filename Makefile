
BUILD_DIR ?= build


.PHONY: gather test report clean doc  docker-mount

gather:
	mrt-config --all . 

docker-mount:
	docker run -it --mount src=$(PWD),target=/workspace,type=bind -w /workspace uprev/mrt 

test: 
	mkdir -p ${BUILD_DIR}
	cd ${BUILD_DIR} && \
	 cmake .. && \
	 make 
	valgrind --leak-check=full --show-leak-kinds=all --log-file=memcheck.log ${BUILD_DIR}/UnitTest/runTests

report:
	${BUILD_DIR}/UnitTest/runTests --gtest_output="xml:unit-test.xml"  #run unit tests

doc: 
	mrt-doc -d doc -c 

clean:
	rm -rf Modules
	rm -rf build


