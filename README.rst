MrT Meta
========

This is the Meta repo for the Up-Rev MrT instance. The module syncing uses the gitlab group structure, so the meta repo is no longer needed to act as a manifest for modules. But it is still used for unit testing and documentation. 


